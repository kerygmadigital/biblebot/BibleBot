/*
* Copyright (C) 2016-2025 Kerygma Digital Co.
*
* This Source Code Form is subject to the terms of the Mozilla Public
* License, v. 2.0. If a copy of the MPL was not distributed with this file,
* You can obtain one at https://mozilla.org/MPL/2.0/.
*/

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using BibleBot.Backend.Controllers.CommandGroups.Information;
using BibleBot.Backend.Controllers.CommandGroups.Resources;
using BibleBot.Backend.Controllers.CommandGroups.Settings;
using BibleBot.Backend.Controllers.CommandGroups.Staff;
using BibleBot.Backend.Controllers.CommandGroups.Verses;
using BibleBot.Backend.Services;
using BibleBot.Backend.Services.Providers;
using BibleBot.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;

namespace BibleBot.Backend.Controllers
{
    [Produces("application/json")]
    [Route("api/commands")]
    [ApiController]
    public class CommandsController(UserService userService, OptOutService optOutService, GuildService guildService, VersionService versionService, ResourceService resourceService,
                              FrontendStatsService frontendStatsService, LanguageService languageService, NameFetchingService nameFetchingService, SpecialVerseProvider svProvider,
                              BibleGatewayProvider bgProvider, APIBibleProvider abProvider, NLTAPIProvider nltProvider, IStringLocalizerFactory localizerFactory) : ControllerBase
    {
        private readonly List<CommandGroup> _commandGroups = [
                new InformationCommandGroup(userService, guildService, versionService, frontendStatsService, localizerFactory),
                new FormattingCommandGroup(userService, guildService, localizerFactory),
                new VersionCommandGroup(userService, guildService, versionService, nameFetchingService, localizerFactory),
                new LanguageCommandGroup(userService, guildService, languageService, localizerFactory),
                new ResourceCommandGroup(resourceService.GetAllResources(), localizerFactory),
                new DailyVerseCommandGroup(userService, guildService, versionService, svProvider, [bgProvider, abProvider, nltProvider], localizerFactory),
                new RandomVerseCommandGroup(userService, guildService, versionService, svProvider, [bgProvider, abProvider, nltProvider], localizerFactory),
                new SearchCommandGroup(userService, guildService, versionService, nameFetchingService, [bgProvider, abProvider, nltProvider], localizerFactory),
                new StaffOnlyCommandGroup()
            ];

        private readonly IStringLocalizer _localizer = localizerFactory.Create(typeof(CommandsController));

        /// <summary>
        /// Processes a message to locate verse references, outputting
        /// the corresponding text.
        /// </summary>
        /// <param name="req">A <see cref="Request" /> object</param>
        /// <response code="200">Returns the corresponding text</response>
        /// <response code="400">If <paramref name="req"/> is invalid</response>
        [Route("process")]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<IResponse>> ProcessMessage([FromBody] Request req)
        {
            OptOutUser potentialOptOut = await optOutService.Get(req.UserId);

            if (potentialOptOut != null)
            {
                return null;
            }

            IResponse response;
            string[] tokenizedBody = req.Body.Split(" ");

            if (tokenizedBody.Length > 0)
            {
                string potentialCommand = tokenizedBody[0];
                string prefix = "+";

                if (potentialCommand.StartsWith(prefix))
                {
                    CommandGroup grp = _commandGroups.FirstOrDefault(grp => grp.Name == potentialCommand.Substring(1));

                    if (grp != null)
                    {
                        string[] staffIds = [
                            "186046294286925824", "270590533880119297", "304602975446499329", // directors
                            "394261640335327234", "1029302033993433130", "842427954263752724" // support specialists
                        ];

                        if (grp.IsStaffOnly && !staffIds.Contains(req.UserId))
                        {
                            return BadRequest(new CommandResponse
                            {
                                OK = false,
                                Pages =
                                [
                                    Utils.GetInstance().Embedify(_localizer["PermissionsErrorTitle"], _localizer["StaffOnlyCommandError"], true)
                                ],
                                LogStatement = $"Insufficient permissions on +{grp.Name}.",
                                Culture = CultureInfo.CurrentUICulture.Name
                            });
                        }

                        // TODO: this logic could be simplified. it wouldn't necessarily optimize anything (nor the opposite),
                        // but it would look visually cleaner
                        if (tokenizedBody.Length > 1)
                        {
                            Command idealCommand = grp.Commands.FirstOrDefault(cmd => cmd.Name == tokenizedBody[1]);

                            if (idealCommand != null)
                            {
                                response = await idealCommand.ProcessCommand(req, [.. tokenizedBody.Skip(2)]);
                                return response.OK ? Ok(response) : BadRequest(response);
                            }
                            else if (grp.Name is "resource" or "search")
                            {
                                response = await grp.DefaultCommand.ProcessCommand(req, [.. tokenizedBody.Skip(1)]);
                                return response.OK ? Ok(response) : BadRequest(response);
                            }
                        }

                        response = await grp.DefaultCommand.ProcessCommand(req, []);
                        return response.OK ? Ok(response) : BadRequest(response);
                    }
                    else
                    {
                        // TODO: this logic could be simplified with above TODO also
                        Command cmd = _commandGroups.FirstOrDefault(grp => grp.Name == "info").Commands.FirstOrDefault(cmd => cmd.Name == potentialCommand.Substring(1));

                        if (cmd != null)
                        {
                            response = await cmd.ProcessCommand(req, []);
                            return response.OK ? Ok(response) : BadRequest(response);
                        }
                    }
                }
            }

            return BadRequest(new CommandResponse
            {
                OK = false,
                Pages = null,
                LogStatement = null
            });
        }
    }
}
